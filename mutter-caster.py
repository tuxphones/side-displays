#!/usr/bin/python3
# Warning: This is an experimental, demo script! Don't rely on it for anything
# Credit: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1698#note_1023511

import sys
import dbus
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst

DBusGMainLoop(set_as_default=True)
Gst.init(None)

loop = GLib.MainLoop()

bus = dbus.SessionBus()
screen_cast_iface = 'org.gnome.Mutter.ScreenCast'
screen_cast_session_iface = 'org.gnome.Mutter.ScreenCast.Session'
screen_cast_stream_iface = 'org.gnome.Mutter.ScreenCast.Session'

screen_cast = bus.get_object(screen_cast_iface,
                             '/org/gnome/Mutter/ScreenCast')
session_path = screen_cast.CreateSession([], dbus_interface=screen_cast_iface)
#print("session path: %s"%session_path)
session = bus.get_object(screen_cast_iface, session_path)

format_element = ""
host = "127.0.0.1"
port = 5000
display_id = 0

if len(sys.argv) != 5:
    print("Usage: %s width height dest_ip port"%sys.argv[0])
    sys.exit(1)

[_, width, height, host, port] = sys.argv
port = int(port)

# to receive:
print("========== To receive on host %s: ========== \n"%(host))
print("$ DISPLAY=:%d gst-launch-1.0 udpsrc port=%d caps=\"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264\" ! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sync=false\n"%(display_id, port))
input("========== First, start the above command on the receiver's side. Then press Enter to launch the stream from this host... ========== ")

print("\nTip: To launch more virtual screens, you can run other instances of this script")

stream_path = session.RecordVirtual(
    dbus.Dictionary({'is-platform': dbus.Boolean(True, variant_level=1),
                        'cursor-mode': dbus.UInt32(1, variant_level=1)}, signature='sv'),
    dbus_interface=screen_cast_session_iface)
format_element = "video/x-raw,max-framerate=60/1,width=%d,height=%d !"%(
    int(width), int(height))

print("stream path: %s"%stream_path)
stream = bus.get_object(screen_cast_iface, stream_path)

pipeline = None

def terminate():
    global pipeline
    print("pipeline: " + str(pipeline))
    if pipeline is not None:
        print("draining pipeline")
        pipeline.send_event(Gst.Event.new_eos())
        pipeline.set_state(Gst.State.NULL)
    print("stopping")
    session.Stop(dbus_interface=screen_cast_session_iface)
    loop.quit()

def on_message(bus, message):
    global pipeline
    type = message.type
    print("message pipeline: " + str(pipeline))
    if type == Gst.MessageType.EOS or type == Gst.MessageType.ERROR:
        terminate()

def on_pipewire_stream_added(node_id):
    print("PipeWire stream initialized")
    global pipeline, host

    pipeline = Gst.parse_launch('pipewiresrc path=%u ! %s videoconvert ! x264enc tune=zerolatency bitrate=5000 speed-preset=superfast ! rtph264pay ! udpsink host=%s port=%d'%(
        node_id, format_element, host, port))
    pipeline.set_state(Gst.State.PLAYING)
    pipeline.get_bus().connect('message', on_message)


stream.connect_to_signal("PipeWireStreamAdded", on_pipewire_stream_added)

session.Start(dbus_interface=screen_cast_session_iface)

print('\n========== ⚠️  Now streaming to %s:%d. To interrupt, close this script or press Ctrl+C. ⚠️  =========='%(host, port))

try:
    loop.run()
except KeyboardInterrupt:
    print("interrupted")
    terminate()
    
